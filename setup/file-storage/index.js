module.exports = {
  buildMetadata: metadata => metadata,
  getPermissions: () => ({
    read: true,
    write: true,
    delete: true
  })
};
