const axios = require("axios");
const _ = require("lodash");

const email = {
  send: async (email, template, data) => {
    await axios.post("http://services-email/api/messages", {
      template,
      data,
      to: [{ email }]
    });
  }
};

const database = {
  add: async user => {
    await axios.post("https://flowtutorial-5041.restdb.io/rest/customers", user, {
      headers: {
        "x-apikey": "c400b5e076696d12f0e600910271aec131f78"
      }
    });
  }
};

const bisnode = {
  getScoreForSSN: async ssn => {
    const { data: refSearch } = await axios.post("http://services-bisnode-credit-pro/creditpro/Personinfo/sokPerson", {
      args: {
        Fodselsnr: ssn
      }
    });

    if (_.isEmpty(refSearch)) return { Beslutning: "Avslag" };

    const internalRef = refSearch[0].ref;

    const { data } = await axios.post("http://services-bisnode-credit-pro/creditpro/Personinfo/hentPersoninfo", {
      args: {
        Internref: internalRef,
        Scoremodell: 1
      }
    });

    return data.Scoring;
  }
};

const services = {
  email,
  database,
  bisnode
};

module.exports = services;
