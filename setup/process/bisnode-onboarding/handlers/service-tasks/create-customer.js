const services = require("../../../common/services");

module.exports = {
  execute: async ({ state }) => {
    await services.database.add(state.user);
    return {};
  },
  map: ({ data }) => {
    return { patch: [], variables: {} };
  }
};
