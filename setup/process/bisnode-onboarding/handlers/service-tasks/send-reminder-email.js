const services = require("../../../common/services");

module.exports = {
  execute: async ({ state }) => {
    await services.email.send("torh@stacc.com", "bisnode-reminder-email", {});

    return {};
  },
  map: ({ data }) => {
    return { patch: [], variables: {} };
  }
};
