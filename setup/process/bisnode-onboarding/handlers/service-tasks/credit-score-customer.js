const services = require("../../../common/services");

module.exports = {
  execute: async ({ state }) => {
    const bisnodeScore = await services.bisnode.getScoreForSSN(state.user.ssn);
    return bisnodeScore;
  },
  map: ({ data }) => {
    return { patch: [{ op: "replace", path: "/bisnode", value: data }], variables: { bisnodeEvaluation: data.Beslutning } };
  }
};
