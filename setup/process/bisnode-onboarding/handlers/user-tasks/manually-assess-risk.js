module.exports = {
  getTaskPermissions: async ({ user, task, context }) => {
    return {
      // The object returned here determines the user's access to the task
      // By default all access is granted unless otherwise specified here
      read: true,
      save: true,
      complete: true,
      delete: true,
      assign: true,
    };
  },
  execute: async ({ state, input }) => {
    return input;
  },
  map: ({ data }) => {
    return { patch: [{ op: "replace", path: "/caseworkerAssessment", value: data }], variables: { isApproved: data.isApproved } };
  },
  getContext: async ({ state, variables }) => {
    return { user: state.user, bisnodeScore: state.bisnode };
  },
  validate: () => ({ valid: true, message: "" })
};