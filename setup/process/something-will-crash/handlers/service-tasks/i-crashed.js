module.exports = {
  execute: async ({ state, variables }) => {
    return { crashErrorMessage: variables.myCrashMessage };
  },
  map: ({ data }) => {
    return { patch: [{ op: "replace", path: "/crash-message", value: data.crashErrorMessage }], variables: {} };
  }
};
