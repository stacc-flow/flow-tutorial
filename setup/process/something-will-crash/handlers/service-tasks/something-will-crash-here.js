module.exports = {
  execute: async ({ state }) => {
    const iWantToCrash = true;
    if (iWantToCrash) {
      throw {
        message: `I wanted to crash`,
        errorCode: "the-crash",
        name: "BpmnError"
      };
    }
    return {};
  },
  map: ({ data }) => {
    return { patch: [], variables: {} };
  }
};
