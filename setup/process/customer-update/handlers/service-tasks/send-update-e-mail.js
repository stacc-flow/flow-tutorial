const services = require("../../../common/services");

module.exports = {
  execute: async ({ state, variables }) => {
    await services.email.send(variables.customer.email, "customer-update-email", {name: variables.customer.name});
    
    return {};
  },
  map: ({ data }) => {
    return { patch: [], variables: {} };
  }
};
