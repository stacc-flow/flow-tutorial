module.exports = {
  execute: async ({ state }) => {
    //mock getting data from DB
    const unUpdatedCustomers = [
      {
        name: "Mr. Hotmail",
        email: "i0257@hotmail.com"
      },
      {
        name: "Ms. Gmail",
        email: "i0257i@gmail.com"
      }
    ];
    return unUpdatedCustomers;
  },
  map: ({ data }) => {
    return { patch: [], variables: {unUpdatedCustomers: data} };
  }
};
