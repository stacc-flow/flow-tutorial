module.exports = {
  execute: async ({ state, input }) => {
    const { name } = input;
    return { name };
  },
  map: ({ data }) => {
    return {
      patch: [{ op: "replace", path: "/name", value: data.name }],
      variables: {}
    };
  },
  getContext: async ({ state, variables }) => {
    return {};
  },
  validate: () => ({ valid: true, message: "" }),
  getTaskPermissions: async ({ user, task, context }) => {
    return {
      // The object returned here determines the user's access to the task
      // By default all access is granted unless otherwise specified here
      read: true,
      save: true,
      complete: true,
      delete: true,
      assign: true,
    };
  },
};
