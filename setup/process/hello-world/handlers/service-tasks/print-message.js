module.exports = {
  execute: async ({ state }) => {
    const { name } = state;
    return { name };
  },
  map: ({ data }) => {
    return {
      patch: [{ op: "replace", path: "/message", value: `Hello ${data.name}!` }],
      variables: {}
    };
  }
};
