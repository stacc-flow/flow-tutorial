const axios = require("axios");
const signingService = axios.create({
  baseURL: "http://services-signing"
});

module.exports = {
  execute: async ({ input }) => {
    // Could retrieve and store pades doc here
    return {};
  },
  map: ({ data }) => {
    return { patch: [{ op: "replace", path: "/status", value: "document-signed" }] };
  },
  validate: async ({ input, state }) => {
    const { requestId } = state.signOrder;
    if (requestId !== input.requestId) {
      return { valid: false, message: "Invalid request id" };
    }
    try {
      const { data } = await signingService.get(`/api/signorder/${requestId}/packaging-status`);
      const { packagingTaskStatusInfo } = data;
      if (packagingTaskStatusInfo[0].packagingTaskStatus === "completed") return { valid: true, message: "" };
      return { valid: false, message: "Signing is not completed" };
    } catch (err) {
      console.error("Could not read packaging-task status", err.message);
      return { valid: false, message: "Could not get signing status" };
    }
  },
  getContext: async ({ state, variables }) => {
    return { url: state.signOrder.signingUrl, requestId: state.signOrder.requestId };
  },
  getTaskPermissions: async ({ user, task, context }) => {
    return {
      // The object returned here determines the user's access to the task
      // By default all access is granted unless otherwise specified here
      read: true,
      save: true,
      complete: true,
      delete: true,
      assign: true,
    };
  },
};
