module.exports = {
  execute: async ({ input }) => input,
  map: ({ data }) => ({
    patch: [],
    variables: {
      manuallyApproved: data.approved
    }
  }),
  getContext: async ({ state, variables }) => {
    return state.customer;
  },
  getTaskPermissions: async ({ user, task, context }) => {
    return {
      // The object returned here determines the user's access to the task
      // By default all access is granted unless otherwise specified here
      read: true,
      save: true,
      complete: true,
      delete: true,
      assign: true,
    };
  },
  validate: () => ({ valid: true, message: ""})
};
