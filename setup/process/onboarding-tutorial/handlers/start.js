module.exports = {
  handle: ({ input }) => {
    return {
      state: {
        customer: {
          ssn: input.ssn,
          email: input.email
        }
      },
      variables: {}
    };
  },
  validate: () => ({ valid: true, message: ""})
};
