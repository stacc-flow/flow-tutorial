const axios = require("axios");
const signingService = axios.create({
  baseURL: "http://services-signing"
});

module.exports = {
  execute: async ({ state, variables }) => {
    const { flowId } = variables;
    const signingPostBody = {
      callbackUrl: `http://localhost:3001/callback/${flowId}`,
      documents: [
        {
          storeId: state.document.storeID,
          description: "Contract",
          sdoType: "pades",
          signees: [
            {
              type: "person",
              signatureMethod: "nbid-sign",
              nationalId: state.customer.ssn
            }
          ]
        }
      ]
    };

    const { data } = await signingService.post(`/api/signorder/simple`, signingPostBody);
    return data;
  },
  map: ({ data }) => {
    const { requestId } = data;
    const signingUrl = data.documents[0].tasks[0].url;
    const signOrder = { signingUrl, requestId };
    return { patch: [{ op: "replace", path: "/signOrder", value: signOrder }] };
  }
};
