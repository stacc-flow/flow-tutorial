const axios = require("axios");
const FormData = require("form-data");

const documentGeneration = axios.create({
  baseURL: "http://services-document-generation"
});
const fileStorage = axios.create({
  baseURL: "http://services-file-storage"
});

module.exports = {
  execute: async ({ state }) => {
    const { customer } = state;
    const { name, age } = customer;
    const dataMapped = {
      inputs: [
        {
          documentTemplateName: "contract",
          data: {
            name,
            age
          },
          isPdfA: false,
          flattenDocument: true
        }
      ]
    };

    const { data: pdf } = await documentGeneration.post("/api/documents/", dataMapped, {
      responseType: "stream"
    });

    const form = new FormData();
    form.append("file", pdf, {
      filename: "contract.pdf"
    });

    const { data: fileReference } = await fileStorage.post("/api/files", form, {
      headers: form.getHeaders()
    });

    return fileReference;
  },
  map: ({ data }) => {
    const [document] = data;
    return {
      patch: [{ op: "replace", path: "/document", value: document }]
    };
  }
};
