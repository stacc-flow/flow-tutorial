const axios = require("axios");

const decisionEvaluator = axios.create({
  baseURL: "http://services-decision-evaluator",
  timeout: 5000
});

module.exports = {
  execute: async ({ state }) => {
    const { data } = await decisionEvaluator.post("/api/decisions/customer-evaluation/evaluate", { age: state.customer.age });
    return data;
  },
  map: ({ data }) => {
    const [{ result }] = data;
    return {
      patch: [{ op: "replace", path: "/evaluationResult", value: result }],
      variables: {
        evaluationResult: result
      }
    };
  }
};
