const axios = require("axios");
const emailService = axios.create({
  baseURL: "http://services-email"
});
module.exports = {
  execute: async ({ state }) => {
    const { customer, signOrder } = state;
    const { email, name } = customer;
    const { signingUrl } = signOrder;

    const postdata = {
      template: "reminder",
      to: [{ email }],
      data: { name, signingUrl }
    };
    await emailService.post("/api/messages", postdata);
    return {};
  },
  map: ({ data }) => {
    return {};
  }
};
