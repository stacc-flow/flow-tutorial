const axios = require("axios");
const bisnode = axios.create({
  baseURL: "http://services-bisnode-credit-pro/creditpro",
  timeout: 10000
});

module.exports = {
  execute: async ({ state }) => {
    const { data: bisnodeResults } = await bisnode.post("/Personinfo/sokPerson", {
      args: {
        Fodselsnr: state.customer.ssn
      }
    });
    return bisnodeResults[0];
  },
  map: ({ data, state }) => {
    const { birthdate, name, address, postalcode, postalplace, age } = data;
    const customer = {
      ...state.customer,
      name,
      address,
      postalCode: postalcode,
      postalPlace: postalplace,
      age,
      birthDate: birthdate
    };
    return {
      patch: [{ op: "replace", path: "/customer", value: customer }]
    };
  }
};
