module.exports = {
  execute: async ({ state, input }) => {
    return { input };
  },
  map: ({ data }) => {
    return { patch: [], variables: { isApproved: data.input.isApproved } };
  },
  getContext: async ({ state, variables }) => {
    return {};
  },
  getTaskPermissions: async ({ user, task, context }) => {
    return {
      // The object returned here determines the user's access to the task
      // By default all access is granted unless otherwise specified here
      read: true,
      save: true,
      complete: true,
      delete: true,
      assign: true,
    };
  },
  validate: () => ({ valid: true, message: ""})
};
