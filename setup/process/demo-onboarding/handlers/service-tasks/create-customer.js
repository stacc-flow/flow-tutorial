const services = require("../../services");

module.exports = {
  execute: async ({ state }) => {
    const user = state.user;
    await services.database.add(user);

    return {};
  },
  map: ({ data }) => {
    return { patch: [], variables: {} };
  }
};
