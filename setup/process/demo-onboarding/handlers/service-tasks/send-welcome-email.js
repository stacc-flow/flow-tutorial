const services = require("../../services");

module.exports = {
  execute: async ({ state }) => {
    await services.email.send(state.user.email, "demo-onboarding-welcome", {
      name: state.user.name
    });

    return {};
  },
  map: ({ data }) => {
    return { patch: [], variables: {} };
  }
};
