module.exports = {
  handle: ({ input }) => {
    return {
      state: { user: input },
      variables: {}
    };
  },
  validate: () => ({ valid: true, message: "" })
};