const axios = require("axios");

const email = {
  send: async (email, template, data) => {
    await axios.post("http://services-email/api/messages", {
      template,
      data,
      to: [{ email }]
    });
  }
};

const database = {
  add: async user => {
    await axios.post(
      "https://flowtutorial-5041.restdb.io/rest/customers",
      user,
      {
        headers: {
          "x-apikey": "c400b5e076696d12f0e600910271aec131f78"
        }
      }
    );
  }
};

const services = {
  email, database
};

module.exports = services
