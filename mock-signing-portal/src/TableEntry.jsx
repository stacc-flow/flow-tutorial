import React, { useEffect, useState } from "react";
import styled from "styled-components";
import axios from "axios";
import SpinnerSmall from './images/spinner-small.svg';

export default function TableEntry(props) {
    const [localLoading, setLocalLoading] = useState(false);
    const { evaluationResult, customer, signOrder } = props.flow.data;

    return (
        <Frame idx={props.idx || 0}>
            <TableTextEntry>{(customer && customer.name) || "-"}</TableTextEntry>
            <TableTextEntry style={{ color: evaluationResult === "GREEN" ? "lightgreen" : evaluationResult }}>
                {evaluationResult || "-"}
            </TableTextEntry>
            <TableTextEntry>{props.flow.status.toUpperCase() || "-"}</TableTextEntry>
            <TableTextEntry>
                {signOrder ? (
                    <a style={{ textDecorationColor: "lightblue", color: "lightblue" }} href={signOrder.signingUrl}>
                        SIGN
                    </a>
                ) : (
                    "PENDING"
                )}
            </TableTextEntry>
            <TableTextEntry>
                {localLoading ? <img style={{ maxHeight: 20, marginLeft: 5 }} src={SpinnerSmall} alt="spinner" /> : (
                    <Button
                        onClick={() => {
                            setLocalLoading(true);
                            async function deleteFlow() {
                                try {
                                    await axios.delete(`/api/flows/${props.flow.flowId}`);
                                } catch(err) {
                                    console.error("Could not delete flow with flowId " + props.flow.flowId);
                                    setLocalLoading(false);
                                }
                                props.getActiveFlows(props.setData);
                            }
                            setTimeout(() => deleteFlow(), 500);
                        }}
                    >
                        DELETE
                    </Button>
                )}
            </TableTextEntry>
        </Frame>
    );
}

const Button = styled.button`
    background-color: #ff000090;
    border: none;
    border-radius: 2px;
    font-size: 12px;
    color: #fff;
    height: 22px;
    font-weight: 500;

    &:hover {
        transition: 0.2s all;
        cursor: pointer;
        opacity: 0.4;
    }
`;

const TableTextEntry = styled.p`
    color: #fff;
    font-size: 14px;
    font-weight: 500;
    margin: 10px;
`;

const Frame = styled.div`
    display: grid;
    min-height: 50px;
    align-items: center;
    grid-template-columns: 2fr 1fr 1fr 1fr 1fr;
    width: 100%;
    background-color: ${props => (props.idx % 2 === 0 ? "#ffffff30" : "#ffffff15")};
`;
