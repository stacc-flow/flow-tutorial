import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import "./index.css";
import * as serviceWorker from "./serviceWorker";

ReactDOM.render(
    <div style={{ height: "100vh", width: "100vw", backgroundImage: "linear-gradient(to right, #ff00cc , #333399)", padding: 0, margin: 0 }}>
        <App />
    </div>,
    document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();

//backgroundImage: "linear-gradient(to right, #ff00cc , #333399)"