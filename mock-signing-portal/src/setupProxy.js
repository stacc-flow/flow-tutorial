const proxy = require("http-proxy-middleware");
module.exports = function(app) {
  app.use(proxy("/api/", { target: `https://api.dev-1.flow-sandbox.stacc.dev`, secure: false, changeOrigin: true }));
};
