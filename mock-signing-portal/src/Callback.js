import axios from "axios";
import React, { useState, useEffect } from "react";
import { Redirect } from "react-router-dom";
import styled from 'styled-components';
import Spinner from "./images/spinner.svg";

export function Callback({ match }) {
    const { flowId } = match.params;
    const [data, setData] = useState({ status: "pending" });
    useEffect(() => {
        async function completeTask() {
            const { data } = await axios.get(`/api/tasks?flowId=${flowId}&taskType=sign-contract`);
            const [task] = data.tasks;
            var urlParams = new URLSearchParams(window.location.search);
            const requestId = urlParams.get("requestId");
            await axios.post(`/api/tasks/${task.taskId}/complete`, { requestId });
            setData({ status: "completed" });
        }
        completeTask();
    }, [flowId]);
    if (data.status === "pending") {
        return (
            <SpinnerFrame>
                <img src={Spinner} alt="spinner" />
            </SpinnerFrame>
        );
    } else {
        return <Redirect to={`/`} />;
    }
}

const SpinnerFrame = styled.div`
    height: 100vh;
    width: 100vw;

    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
`;
