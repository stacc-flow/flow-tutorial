import React, { useState, useEffect } from "react";
import styled from "styled-components";
import axios from "axios";
import TableEntry from "./TableEntry";
import TableHeader from "./TableHeader";
import Spinner from "./images/spinner.svg";

async function getActiveFlows() {
    const { data } = await axios.get(`/api/flows?status=active`);
    const { flows } = data;
    return flows;
}

async function fetchFlows(setData) {
    try {
        const flows = await getActiveFlows();
        setData({ flows: flows || [], loading: false });
    }catch(err) {
        setData({ flows: [], loading: false });
    }
}

export default function Overview() {
    const [data, setData] = useState({ flows: [], loading: true });
    useEffect(() => {
        setTimeout(() => fetchFlows(setData), 1000);
    }, []);
    if (data.loading) {
        return (
            <Frame>
                <Content style={{ justifyContent: "center" }}>
                    <img src={Spinner} alt="spinner" />
                </Content>
            </Frame>
        );
    }

    return (
        <Frame>
            <Content>
                <Title>{data.flows.length} Active processes</Title>
                <InnerContent>
                    <TableHeader />
                    {data.flows.length ? (
                        data.flows.map((flow, idx) => (
                            <TableEntry key={idx} getActiveFlows={() => fetchFlows(setData)} flow={flow} idx={idx} />
                        ))
                    ) : (
                        <ErrorText>There are no contracts to be signed</ErrorText>
                    )}
                </InnerContent>
            </Content>
        </Frame>
    );
}

const ErrorText = styled.p`
    color: #fff;
    font-size: 16px;
    font-weight: 500;
    margin-top: 20px;
`;

const Title = styled.h1`
    font-size: 24px;
    font-weight: 500;
    color: #fff;
    margin-top: 10px;
    margin-bottom: 20px;
    padding-top: 0;
`;

const Content = styled.div`
    min-height: 600px;
    width: 50px;
    width: 50%;
    min-width: 500px;
    max-width: 100%;

    display: flex;
    flex-direction: column;
    align-items: center;
`;

const InnerContent = styled.div`
    width: 100%;
    height: 100%;
    border: 1px solid #ffffff15;
    min-width: 100%;

    display: flex;
    flex-direction: column;
    align-items: center;
`;

const Frame = styled.div`
    height: 100vh;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
`;
