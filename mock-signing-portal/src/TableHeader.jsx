import React from "react";
import styled from "styled-components";

export default function TableEntry(props) {
    return (
        <Frame idx={props.idx || 0}>
            <TableTextEntry>Name</TableTextEntry>
            <TableTextEntry>Evaluation result</TableTextEntry>
            <TableTextEntry>Status</TableTextEntry>
            <TableTextEntry>Sign order</TableTextEntry>
            <TableTextEntry>Delete</TableTextEntry>
        </Frame>
    );
}

const TableTextEntry = styled.p`
    color: #fff;
    font-size: 12px;
    font-weight: 300;
    margin: 10px;
`;

const Frame = styled.div`
    display: grid;
    min-height: 20px;
    align-items: center;
    grid-template-columns: 2fr 1fr 1fr 1fr 1fr;
    width: 100%;
`;
