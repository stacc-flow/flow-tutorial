import React from "react";
import "./index.css";
import { BrowserRouter as Router, Route } from "react-router-dom";
import Overview from "./Overview";
import { Callback } from "./Callback";

function AppRouter() {
  return (
    <Router>
      <div>
        <Route path="/" exact component={Overview} />
        <Route path="/callback/:flowId" component={Callback} />
      </div>
    </Router>
  );
}

export default AppRouter;
