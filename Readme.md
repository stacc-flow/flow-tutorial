# Flow Tutorial

Maintainer: Øyvind Fanebust ([slack](https://stacc-as.slack.com/team/U42FE3UHE), [email](mailto:oyvindf@stacc.com))

## Requirements

- Camunda modeler (Flow Studio coming soon!)
- Visual Studio Code
- Postman
- node (recent version)

## Installing Flow CLI

```bash
# Install flow cli
curl -sSL https://flow-cli.stacc.dev/install.sh | bash

# Authenticate using e-mail OTP:
flow login --url https://k8s.login.flow-sandbox-1.stacc.dev --provider email-otp --issuer https://oidc.login.flow-sandbox-1.stacc.dev

# Continuous deployment
flow push setup -w
```

### Process Wizard

Use the process wizard to test your process at https://wizard.{ENV}.flow-sandbox.stacc.dev

### Signing Portal

- Point the proxy to your environment. In mock-signing-portal/src/setupProxy.js change the URL to your corresponding environment.
- Run `yarn`
- Run `yarn start`

## Test data

- 01065100394 Atle Strømstad (gul)
- 08079933938 Jarle Hammarfest (grønn)
